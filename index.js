// solution #2

db.users.find(
	{
		$or: [
			{firstName: {$regex: /s/i}}, // $regex: "s", $options: "i"
			{lastName: {$regex: /d/i}}
			]
	},
	{
		_id: 0,
		firstName: 1,
		lastName: 1
	}
)


// solution #3 
db.users.find(
	{
		$and: [
			{department: "HR"},
			{age: {$gte: 70}}
		]
	}
)


// solution #4
db.users.find(
	{
		$and: [{firstName: {$regex: /e/i}},{age: {$lte: 30}}]
	}
)

